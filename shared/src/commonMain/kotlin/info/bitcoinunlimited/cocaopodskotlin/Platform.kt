package info.bitcoinunlimited.cocaopodskotlin

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform